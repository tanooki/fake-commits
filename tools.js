const gitlab = require('./gitlab').gitlab
const urlEncode = require('./gitlab').urlEncodedPath
const dedent = require('./gitlab').dedent

// === API ===
// https://docs.gitlab.com/ee/api/repositories.html
// GET /projects/:id/repository/tree

async function repositoryTree({projectId, perPage, page, token}) {
  return gitlab({
    method: "GET", 
    path: `/projects/${urlEncode({name:projectId})}/repository/tree/?per_page=${perPage}&page=${page}`, 
    token: token,  
    data: null
  })
}

// https://docs.gitlab.com/ee/api/repository_files.html#create-new-file-in-repository
// POST /projects/:id/repository/files/:file_path
/*
curl --request POST --header 'PRIVATE-TOKEN: <your_access_token>' --header "Content-Type: application/json" \
  --data '{"branch": "master", "author_email": "author@example.com", "author_name": "Firstname Lastname", \
    "content": "some content", "commit_message": "create a new file"}' \
  "https://gitlab.example.com/api/v4/projects/13083/repository/files/app%2Fproject%2Erb"

*/
async function repositoryCreateFile({projectId, filePath, branch, authorEmail, authorName, content, commitMessage, token}) {
  return gitlab({
    method:"POST",
    path: `/projects/${urlEncode({name:projectId})}/repository/files/${urlEncode({name:filePath})}`,
    token: token, 
    data: {
      branch: branch,
      author_email: authorEmail,
      author_name: authorName, 
      content: content,
      commit_message: commitMessage
    }
  })
}

// PUT /projects/:id/repository/files/:file_path
async function repositoryUpdateFile({projectId, filePath, branch, authorEmail, authorName, content, commitMessage, token}) {
  return gitlab({
    method:"PUT",
    path: `/projects/${urlEncode({name:projectId})}/repository/files/${urlEncode({name:filePath})}`,
    token: token, 
    data: {
      branch: branch,
      author_email: authorEmail,
      author_name: authorName, 
      content: content,
      commit_message: commitMessage
    }
  })
}

// TODO: Delete file: https://docs.gitlab.com/ee/api/repository_files.html#delete-existing-file-in-repository

// Create repository branch: https://docs.gitlab.com/ee/api/branches.html#create-repository-branch
// POST /projects/:id/repository/branches
// curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/repository/branches?branch=newbranch&ref=master"

async function repositoryCreateBranch({projectId, branch, ref, token}) {
  return gitlab({
    method:"POST",
    path: `/projects/${urlEncode({name:projectId})}/repository/branches`,
    token: token, 
    data: {
      branch: branch,
      ref: ref
    }
  })
}

// Create a MR: https://docs.gitlab.com/ee/api/merge_requests.html#create-mr
// POST /projects/:id/merge_requests

async function repositoryCreateMergeRequest({projectId, sourceBranch, targetBranch, title, token}) {
  return gitlab({
    method:"POST",
    path: `/projects/${urlEncode({name:projectId})}/merge_requests`,
    token: token, 
    data: {
      source_branch: sourceBranch,
      target_branch: targetBranch,
      title: title
    }
  })
}

// Merge to default merge ref path
// GET /projects/:id/merge_requests/:merge_request_iid/merge_ref
async function repositoryMergeMergeRequest({projectId, mergeRequestIid, token}) {
  return gitlab({
    method:"GET",
    path: `/projects/${urlEncode({name:projectId})}/merge_requests/${mergeRequestIid}/merge_ref`,
    token: token, 
    data: null
  })
}
// Accept Merge request: https://docs.gitlab.com/ee/api/merge_requests.html#accept-mr
// PUT /projects/:id/merge_requests/:merge_request_iid/merge

async function repositoryAcceptMergeRequest({projectId, mergeRequestIid, token}) {
  return gitlab({
    method:"PUT",
    path: `/projects/${urlEncode({name:projectId})}/merge_requests/${mergeRequestIid}/merge`,
    token: token, 
    data: {
      should_remove_source_branch: true
    }
  })
}

// *** Pipelines ***

// https://docs.gitlab.com/ee/api/pipelines.html#list-project-pipelines
// GET /projects/:id/pipelines
async function projectPipelinesList({projectId, token}) {
  return gitlab({
    method:"GET",
    path: `/projects/${urlEncode({name:projectId})}/pipelines`,
    token: token, 
    data: null
  })
}
// https://docs.gitlab.com/ee/api/pipelines.html#delete-a-pipeline
// DELETE /projects/:id/pipelines/:pipeline_id
async function projectDeletePipeline({projectId, pipelineId, token}) {
  return gitlab({
    method:"DELETE",
    path: `/projects/${urlEncode({name:projectId})}/pipelines/${pipelineId}`,
    token: token, 
    data: null
  })
}

// -------------------------------------------------
//  Helpers 
// -------------------------------------------------

async function tree(projectId, token) {
  return await repositoryTree({
    projectId: projectId,
    perPage: 30,
    page: 1,
    token, token
  })
  .then(response => response.data)
  .then(files => {
    //console.log("😃", files)
    return {failure: undefined, success: files}
  }).catch(error => {
    //console.error("😡", error.response)
    return {failure: error.response.data, success: undefined}
  })
}

async function createFile({projectId, filePath, branch, authorEmail, authorName, content, commitMessage, token}) {
  return await repositoryCreateFile({projectId, filePath, branch, authorEmail, authorName, content, commitMessage, token})
  .then(response => response.data)
  .then(result => {return {failure: undefined, success: result}})
  .catch(error => {return {failure: error.response.data, success: undefined}})
}

async function updateFile({projectId, filePath, branch, authorEmail, authorName, content, commitMessage, token}) {
  return await repositoryUpdateFile({projectId, filePath, branch, authorEmail, authorName, content, commitMessage, token})
  .then(response => response.data)
  .then(result => {return {failure: undefined, success: result}})
  .catch(error => {return {failure: error.response.data, success: undefined}})
}

async function createBranch({projectId, branch, ref, token}) {
  return await repositoryCreateBranch({projectId, branch, ref, token})
  .then(response => response.data)
  .then(result => {return {failure: undefined, success: result}})
  .catch(error => {return {failure: error.response.data, success: undefined}})
}

async function createMergeRequest({projectId, sourceBranch, targetBranch, title, token}) {
  return await repositoryCreateMergeRequest({projectId, sourceBranch, targetBranch, title, token})
  .then(response => response.data)
  .then(result => {return {failure: undefined, success: result}})
  .catch(error => {return {failure: error.response.data, success: undefined}})
}

async function merge({projectId, mergeRequestIid, token}) {
  return await repositoryMergeMergeRequest({projectId, mergeRequestIid, token})
  .then(response => response.data)
  .then(result => {return {failure: undefined, success: result}})
  .catch(error => {return {failure: error.response.data, success: undefined}})
}

async function acceptMergeRequest({projectId, mergeRequestIid, token}) {
  return await repositoryAcceptMergeRequest({projectId, mergeRequestIid, token})
  .then(response => response.data)
  .then(result => {return {failure: undefined, success: result}})
  .catch(error => {return {failure: error.response.data, success: undefined}})
}

async function pipelinesList({projectId, token}) {
  return await projectPipelinesList({projectId, token})
  .then(response => response.data)
  .then(result => {return {failure: undefined, success: result}})
  .catch(error => {return {failure: error.response.data, success: undefined}})
}

async function deletePipeline({projectId, pipelineId, token}) {
  return await projectDeletePipeline({projectId, pipelineId, token})
  .then(response => response.data)
  .then(result => {return {failure: undefined, success: result}})
  .catch(error => {return {failure: error.response.data, success: undefined}})
}


// +++ Main +++
// this is an example
async function batch() {

  let user = {
    token: process.env.SWANNOU_TOKEN,
    email: "ph.charriere+swannou@gmail.com",
    name: "swannou"
  }

  let repoTree_demo = await tree("tanuki-workshops/merge-trains/demo", process.env.SWANNOU_TOKEN)
  if(repoTree_demo.failure) {
      console.log("😡", repoTree_demo.failure)
  } else {
    console.log("😃", repoTree_demo.success)
  }
  //let repoTree_fakeCommits = await tree("tanuki-workshops/merge-trains/fake-commits", process.env.SWANNOU_TOKEN)
  //console.log(repoTree_fakeCommits)

  let helloFile = await createFile({
    projectId: "tanuki-workshops/merge-trains/demo",
    filePath: "hello.md",
    branch: "master",
    authorEmail: user.email, authorName: user.name, 
    content: `this is a test \n hello 😃 \n`,
    commitMessage: "create hello file",
    token: user.token
  })

  console.log(helloFile)

  let helloFileUpdate = await updateFile({
    projectId: "tanuki-workshops/merge-trains/demo",
    filePath: "hello.md",
    branch: "master",
    authorEmail: user.email, authorName: user.name,
    content: dedent
    `this is a test
     Hello world
     - 👋
     - 😉
       - Hello
    `,
    commitMessage: "update hello file",
    token: user.token
  })

  console.log(helloFileUpdate)


  let newBranch = await createBranch({
    projectId: "tanuki-workshops/merge-trains/demo",
    branch: "my-feature-branch",
    ref: "master",
    token: user.token
  })
  if(newBranch.failure) {
    console.log("😡 error when creating branch", newBranch.failure)
  } else {
    console.log("👋 New branch created: ", newBranch.success.name)

    // then create a Merge Request
    let myMergeRequest = await createMergeRequest({
      projectId: "tanuki-workshops/merge-trains/demo",
      title: "this is my first MR 😃",
      targetBranch: "master",
      sourceBranch: newBranch.success.name,
      token: user.token
    })

    console.log("🧪", myMergeRequest)

    // then create a file
    let heyFile = await createFile({
      projectId: "tanuki-workshops/merge-trains/demo",
      filePath: "hey.md",
      branch: newBranch.success.name,
      authorEmail: user.email, authorName: user.name,
      content: dedent
        `
        this is the **hey.md** file 📝 
        `,
       commitMessage: "create hello file",
       token: user.token
    })
    console.log("📝", heyFile)
  }

}

//batch()

async function MR({projectId, featureBranchName, mergeRequesTitle, filePath, content, commitMessage, user}) {

  let newBranch = await createBranch({
    projectId: projectId,
    branch: featureBranchName,
    ref: "master",
    token: user.token
  })

  if(newBranch.failure) {
    console.log("😡 error when creating branch", newBranch.failure)

    return {failure: "error when creating branch", success: undefined}

  } else {
    console.log("👋 new branch created: ", newBranch.success.name)

    // then create a Merge Request
    let newMergeRequest = await createMergeRequest({
      projectId: projectId,
      title: mergeRequesTitle,
      targetBranch: "master",
      sourceBranch: newBranch.success.name,
      token: user.token
    })

    if(newMergeRequest.failure) {
      console.log("😡 error when creating merge request", newMergeRequest.failure)

      return {failure: "error when creating merge request", success: undefined}
    } else {
      console.log("👋 merge request created: ", newMergeRequest.success.id, newMergeRequest.success.iid, newMergeRequest.success.title) // 🤔
      
      // then create a file
      let newFile = await createFile({
        projectId: projectId,
        filePath: filePath,
        branch: newBranch.success.name,
        authorEmail: user.email, authorName: user.name,
        content: content,
        commitMessage: commitMessage,
        token: user.token
      })
      if(newFile.failure) {
        console.log("😡 error when creating file", newFile.failure)
        return {failure: "error when creating file", success: undefined}
      } else {
        console.log("👋 new file created: ", newFile.success) // 🤔
      }
      //await new Promise(resolve => setTimeout(resolve, 5000))

      /*
      let mergedBranch = await merge({
        projectId: projectId,
        mergeRequestIid: newMergeRequest.success.iid, 
        token: user.token
      })
      console.log("🔴 mergedBranch", mergedBranch)
      */
      return {failure: undefined, success: {id:newMergeRequest.success.id, iid:newMergeRequest.success.iid}}
      
      //return {id:newMergeRequest.success.id, iid:newMergeRequest.success.iid, }
    }
  }
}

async function acceptMR({projectId, mergeRequestIid, user}) {
  let acceptMR = await acceptMergeRequest({
    projectId: projectId,
    mergeRequestIid: mergeRequestIid, 
    token: user.token
  }) 
  if(acceptMR.failure) {
    console.log("😡 error when accepting merge request", acceptMR.failure)
    return {failure: "error when accepting merge request", success: undefined}
  } else {
    console.log("👋 merge request accepted: ", acceptMR.success.state) // 🤔
    return {failure: undefined, success: acceptMR.success.state}
  }
}

module.exports = {
  MR: MR,
  pipelinesList: pipelinesList,
  deletePipeline: deletePipeline,
  acceptMR: acceptMR

}

