#!/usr/bin/env node

const pipelinesList = require("./tools").pipelinesList
const deletePipeline = require("./tools").deletePipeline

let projectId = "tanuki-workshops/merge-trains/demo-no-train"
let token = process.env.GITLAB_TOKEN_ADMIN

async function run() {
  let pipelines = await pipelinesList({projectId: projectId, token: token})
  //console.log(pipelines.success)
  for(var index in pipelines.success) {
    console.log("Pipeline id", pipelines.success[index].id)
    let remove = await deletePipeline({projectId: projectId, pipelineId: pipelines.success[index].id, token: token})
    console.log(remove)
  }

}

run()

