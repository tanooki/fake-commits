#!/usr/bin/env node

const MR = require("./tools").MR
const dedent = require('./gitlab').dedent

async function run() {
  let mergeRequest = await MR({
    projectId: "tanuki-workshops/merge-trains/demo", 
    featureBranchName: "ft-add-hello-hello", 
    mergeRequesTitle: "add some text to hello file",
    filePath: "hello-bis.md",
    content: dedent`
    👋 Hello World 🌍 
    `,
    commitMessage:"add new file 📝",
    user: {
      token: process.env.SWANNOU_TOKEN,
      email: "ph.charriere+swannou@gmail.com",
      name: "swannou"      
    }
  })
  // https://docs.gitlab.com/ee/api/merge_requests.html#merge-to-default-merge-ref-path
  
  console.log(mergeRequest)
}

run()
