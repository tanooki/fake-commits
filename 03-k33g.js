#!/usr/bin/env node

const MR = require("./tools").MR
const dedent = require('./gitlab').dedent

async function run() {
  let mergeRequest = await MR({
    projectId: "tanuki-workshops/merge-trains/demo", 
    featureBranchName: "ft-add-hi-again", 
    mergeRequesTitle: "add some text to hi file",
    filePath: "hi.md",
    content: dedent`
    👋 Hi I'm Bob
    `,
    commitMessage:"add new file 📝",
    user: {
      token: process.env.GITLAB_TOKEN_ADMIN,
      email: "pcharriere@gitlab.com",
      name: "k33g"    
    }
  })
  
  console.log(mergeRequest)
}
run()


