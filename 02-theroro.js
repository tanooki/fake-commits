#!/usr/bin/env node

const MR = require("./tools").MR
const dedent = require('./gitlab').dedent

async function run() {
  let mergeRequest = await MR({
    projectId: "tanuki-workshops/merge-trains/demo", 
    featureBranchName: "ft-add-hey-again", 
    mergeRequesTitle: "add some text to hey file",
    filePath: "hey.md",
    content: dedent`
    👋 Hey People 😃 
    How are you ?
    Oh Oh this is a 🍄
    `,
    commitMessage:"add new file 📝",
    user: {
      token: process.env.THERORO_TOKEN,
      email: "ph.charriere+theroro@gmail.com",
      name: "theroro"      
    }
  })
  
  console.log(mergeRequest)
}
run()

