const axios = require("axios")

String.prototype.replaceAll = function(search, replacement) {
  var target = this
  return target.split(search).join(replacement)
}

function urlEncodedPath({ name }){
    let encoded = `${name
        .replaceAll("/", "%2F")
        .replaceAll(".", "%2E")
        .replaceAll("-", "%2D")
        .replaceAll("_", "%5F")
        .replaceAll(".", "%2E")}`;
    //console.log("👋", encoded);
    return encoded;
}

function gitlab({method, path, data, token}) {
  //let gitlab_token = process.env.GITLAB_TOKEN_ADMIN || token
  let gitlab_token = token

  let gitlabUrl = process.env.GITLAB_URL || "https://gitlab.com"
  let headers = {
    "Content-Type": "application/json",
    "Private-Token": gitlab_token
  }
  return axios({
    method: method,
    url: gitlabUrl + '/api/v4' + path,
    headers: headers,
    data: data !== null ? JSON.stringify(data) : null
  })
}

function dedent(callSite, ...args) {

    function format(str) {

        let size = -1;

        return str.replace(/\n(\s+)/g, (m, m1) => {

            if (size < 0)
                size = m1.replace(/\t/g, "    ").length;

            return "\n" + m1.slice(Math.min(m1.length, size));
        });
    }

    if (typeof callSite === "string")
        return format(callSite);

    if (typeof callSite === "function")
        return (...args) => format(callSite(...args));

    let output = callSite
        .slice(0, args.length + 1)
        .map((text, i) => (i === 0 ? "" : args[i - 1]) + text)
        .join("");

    return format(output);
}

module.exports = {
  urlEncodedPath: urlEncodedPath,
  gitlab: gitlab,
  dedent: dedent
}